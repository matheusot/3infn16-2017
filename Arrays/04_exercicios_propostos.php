<?php

/***************
 * Exercício 01 *
 ***************/
echo "Exercício 01: ";

function mediaDeArray($notas, $pesos) {
    $resultado = 0;

    for($x=0; $x<sizeof($notas); $x++) {
        $resultado += ($notas[$x] / 10) * $pesos[$x];
    }

    return $resultado;

}
echo mediaDeArray(array(7, 8, 10), array(2, 3, 5));

echo "<BR><BR>";



/***************
 * Exercício 02 *
 ***************/
echo "Exercício 02: ";

function inverteArray($array) {
    $novoArray = array();

    for($x=sizeof($array) - 1; $x>=0; $x--) {
        $novoArray[sizeof($array) - 1 - $x] = $array[$x];
    }

    return $novoArray;

}
print_r(inverteArray(array("a", "b", "c", "d")));

echo "<BR><BR>";



/***************
 * Exercício 03 *
 ***************/
echo "Exercício 03: ";

function tamanhoDeArray($array) {
    $x = 0;
    foreach($array as $posicao) {
        $x++;
    }
    return $x;
}
echo tamanhoDeArray(array(1, 2, 3));

echo "<BR><BR>";



/***************
 * Exercício 04 *
 ***************/
echo "Exercício 04: ";

function meu_array_push($array, $elemento) {

    /*
    * Para inserir um elemento no final do array basta informar a chave do novo
    * valor entre colchetes (neste caso foi utilizada uma chave numérica) e
    * atribuir nesta posição o valor ou elemento em questão.
    */
    $array[sizeof($array)] = $elemento;
    return $array;
}
$x = array(1, 2, 3);
$x = meu_array_push($x, 4);
print_r($x);

echo "<BR><BR>";



/***************
 * Exercício 05 *
 ***************/
echo "Exercício 05: ";

function meu_array_pop(&$array) {
    $elemento = $array[sizeof($array) - 1];
    unset($array[sizeof($array) - 1]);
    return $elemento;

}
$x = array(1, 2, 3);
echo meu_array_pop($x);
print_r($x);

echo "<BR><BR>";



/***************
 * Exercício 06 *
 ***************/
echo "Exercício 06: ";

function meu_array_shift($array) {
    $elemento = $array[0];
    unset($array[0]);
    return $elemento;

}
$x = array(1, 2, 3);
echo meu_array_shift($x);
print_r($x);

echo "<BR><BR>";



/***************
 * Exercício 07 *
 ***************/
echo "Exercício 07: ";

function meu_array_unshift($array, $elemento) {

    /*
    * Antes de inserir o elemento na primeira posição, todos os atuais elementos
    * devem ser reposicionados. Por isto o comando for é montado invertido (do
    * fim para o início) reposicionando todos os elementos primeiramente.
    */
    for($x=sizeof($array); $x>0; $x--) {
        $array[$x] = $array[$x-1];
    }

    $array[0] = $elemento;
    return $array;
}
$x = array(1, 2, 3);
$x = meu_array_unshift($x, 4);
print_r($x);

echo "<BR><BR>";



/***************
 * Exercício 08 *
 ***************/
echo "Exercício 08: ";

function minha_array_key_exists($chaveBusca, $array) {
    $chaves = array_keys($array);

    foreach($chaves as $chave) {
        if($chave == $chaveBusca) {
            return TRUE;
        }
    }
    return FALSE;
}
$x = array("Empresa" => "Softblue", "Nome" => "Andre");
echo minha_array_key_exists("Empresa", $x);
echo minha_array_key_exists("Softblue", $x);

?>
