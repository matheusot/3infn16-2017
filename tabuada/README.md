# LEIA-ME!!! #

Criar uma aplicação que calcule e apresenta a tabuada segundo a escolha do usuário, conforme descrito na imagem \Docs\Tabuada_WireFrame.png

### Identificação da autoria ###

* A aplicação pode ser feita em dupla
* Todo arquivo deverá possuir um cabeçalho (comentário) identificando o(s) autor(es), informando RA, Nome, Data e uma descrição do objetivo do arquivo)
